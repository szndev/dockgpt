FROM ubuntu:latest

# Install required dependencies
RUN dpkg --add-architecture arm64 && \
    apt-get update && \
    apt-get install -y openjdk-17-jdk git unzip wget curl gnupg2 software-properties-common file && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install Android Command Line Tools
ARG ANDROID_SDK_ROOT=/opt/android-sdk
ENV ANDROID_SDK_ROOT ${ANDROID_SDK_ROOT}
ARG ANDROID_HOME=${ANDROID_SDK_ROOT}
ENV ANDROID_HOME ${ANDROID_HOME}
RUN mkdir -p ${ANDROID_SDK_ROOT}/cmdline-tools/latest && cd ${ANDROID_SDK_ROOT}/cmdline-tools/latest && \
    wget https://dl.google.com/android/repository/commandlinetools-linux-7583922_latest.zip && \
    unzip commandlinetools-linux-7583922_latest.zip && \
    mv cmdline-tools/* ${ANDROID_SDK_ROOT}/cmdline-tools/latest && \
    rm -r cmdline-tools && \
    rm commandlinetools-linux-7583922_latest.zip && \
    yes | ${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin/sdkmanager --licenses

# Install Fastlane
RUN curl -sL https://get.rvm.io | bash -s stable && \
    /bin/bash -c "source /etc/profile.d/rvm.sh && rvm install 2.7 && gem install fastlane -NV"

# Accept Android licenses
RUN yes | ${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin/sdkmanager --licenses

# Set up environment variables
ENV PATH ${PATH}:${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin:${ANDROID_SDK_ROOT}/platform-tools

# Set the default command to be a shell
CMD ["/bin/bash"]
